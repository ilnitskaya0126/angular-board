import { Component, OnInit } from '@angular/core';
import { Column } from '../column/column.module';
import { NgModel} from '@angular/forms';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';



@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {

  columnList: Column[] = [];
  column: Column = new Column("")

  ngOnInit(): void {
  }

  addColumn(title:NgModel) {
    this.columnList.push(new Column(title.model))
  }

   drop(event: CdkDragDrop<string[]>) {
     
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }


}
