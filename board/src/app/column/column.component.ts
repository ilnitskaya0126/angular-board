import { Component, OnInit, Input} from '@angular/core';
import { Card } from '../card/card.module';
import { NgModel} from '@angular/forms';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';


@Component({
  selector: 'app-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.css']
})
export class ColumnComponent implements OnInit {

  @Input() title: string
  cardList: Card [] = []
  card: Card = new Card("","");
 
  addCard(title:NgModel, description:NgModel){
    this.cardList.push(new Card(title.model,description.model));
    console.log(this.cardList)
  }

  ngOnInit(): void {
  }


  removeCard(index:number){
    if(index > -1){
      this.cardList.splice(index,1);
    }

  }

  drop(event: CdkDragDrop<string[]>) {
    console.log('hi')
        console.log(event.previousContainer,
                        event.container,
                        event.previousIndex,
                        event.currentIndex)

    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }

}
