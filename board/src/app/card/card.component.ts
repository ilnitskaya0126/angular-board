import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input() title: string;
  @Input() description: string;
  commentList: string[] = [];
  like: boolean = false;
  showInputComment: boolean = false;

  showForm() {
    this.showInputComment = true;
  }

  likeCard() {
    this.like = true;
  }

  unlikeCard() {
    this.like = false;
  }

  addComment(event) {
    this.commentList.push(event.target.comment.value);
    event.target.comment.value = '';
    this.showInputComment = false;
  }

}
